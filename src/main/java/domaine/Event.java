package domaine;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Event {
    private UUID id;
    private LocalDate startDate;
    private String description;

    public Event(UUID id, LocalDate startDate, String description) {
        this.id = id;
        this.startDate = Objects.requireNonNull(startDate);
        this.description = Objects.requireNonNull(description);
    }


    public UUID getId() {
        return id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = Objects.requireNonNull(description);
    }
}
