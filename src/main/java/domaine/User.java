package domaine;

import java.util.Objects;

public class User {
    private String login;
    private String hashedPassword;

    public User(String login, String hashedPassword) {
        this.login = Objects.requireNonNull(login);
        this.hashedPassword = Objects.requireNonNull(hashedPassword);
    }
    public String getLogin() {
        return login;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }
}
