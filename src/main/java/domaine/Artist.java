package domaine;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Artist {
    private UUID id;
    private String name;
    private LocalDate careerStart;
    private List<Event> events;
    private String description;

    public Artist(UUID id, String name, LocalDate careerStart, String description, List<Event> events) {
        this.id = Objects.requireNonNull(id);
        this.name = Objects.requireNonNull(name);
        this.careerStart = Objects.requireNonNull(careerStart);
        this.events = Objects.requireNonNull(events);
        this.description = Objects.requireNonNull(description);
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = Objects.requireNonNull(name);
    }

    public List<Event> getEvents() {
        return events;
    }

    public LocalDate getCareerStart() {
        return careerStart;
    }


    public void setCareerStart(LocalDate careerStart) {
        this.careerStart = careerStart;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = Objects.requireNonNull(description);
    }

    public void addEventToEventList(Event event) {
        this.events.add(event);
    }
}

