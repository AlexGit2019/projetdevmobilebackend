package services;

import domaine.Event;

import java.util.List;
import java.util.UUID;

public interface EventStorageInterface {
    List<Event> getEvents(UUID specifiedEvent);
    boolean addEvent(Event eventToInsert);

    boolean updateEvent(Event event);

    boolean deleteArtist(UUID eventUuid);
}
