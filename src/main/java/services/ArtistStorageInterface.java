package services;

import domaine.Artist;
import domaine.User;

import java.util.List;
import java.util.UUID;

public interface ArtistStorageInterface {
    List<Artist> getArtistsList(UUID specifiedArtistId);
    boolean insertArtist(Artist artistToInsert);
    boolean updateArtist(Artist artist);
    boolean deleteArtist(UUID artistUuid);
}
