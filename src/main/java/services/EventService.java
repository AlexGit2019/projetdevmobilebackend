package services;

import domaine.Event;

import java.util.List;
import java.util.UUID;

public class EventService {
    private EventStorageInterface eventStorageInterface;

    public EventService(EventStorageInterface eventStorageInterface) {
        this.eventStorageInterface = eventStorageInterface;
    }
    public List<Event> getEvents(UUID specifiedEventId) {
        return eventStorageInterface.getEvents(specifiedEventId);
    }
    public boolean addEvent(Event eventToInsert) {
        return this.eventStorageInterface.addEvent(eventToInsert);
    }

    public boolean updateEvent(Event event) {
        return eventStorageInterface.updateEvent(event);
    }

    public boolean deleteArtist(UUID eventUuid) {
        return eventStorageInterface.deleteArtist(eventUuid);
    }
}
