package services;

import domaine.User;

public interface UserStorageInterface {
    Boolean authenticateUser(User user);
}
