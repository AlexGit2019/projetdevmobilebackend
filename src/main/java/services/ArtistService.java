package services;

import domaine.Artist;

import java.util.List;
import java.util.UUID;

public class ArtistService {
    private ArtistStorageInterface artistStorageInterface;

    public ArtistService(ArtistStorageInterface artistStorageInterface) {
        this.artistStorageInterface = artistStorageInterface;
    }

    public List<Artist> getArtistList(UUID specifiedArtistId) {
        return artistStorageInterface.getArtistsList(specifiedArtistId);
    }
    public boolean deleteArtist(UUID artistUuid) {
        return this.artistStorageInterface.deleteArtist(artistUuid);
    }
    public boolean insertArtist(Artist artist) {
        return artistStorageInterface.insertArtist(artist);
    }
    public boolean updateArtist(Artist artist) {
        return artistStorageInterface.updateArtist(artist);
    }
}
