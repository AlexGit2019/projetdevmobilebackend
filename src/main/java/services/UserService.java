package services;

import domaine.User;

public class UserService {
    private UserStorageInterface stockageUser;

    public UserService(UserStorageInterface stockageUser) {
        this.stockageUser = stockageUser;
    }

    public Boolean authenticateUser(User user) {
        return stockageUser.authenticateUser(user);
    }
}
