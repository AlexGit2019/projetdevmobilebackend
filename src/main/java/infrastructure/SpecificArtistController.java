package infrastructure;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import domaine.Artist;
import infrastructure.mysql.ArtistStorageMySQL;
import org.mapstruct.factory.Mappers;
import services.ArtistService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SpecificArtistController extends ItemRelatedHttpServlet implements PatchInterface, ServletErrorMessageInterface {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        boolean[] superCalled = {false};
        ServletService superProxy = new ServletService() {
            @Override
            public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                superCalled[0] = true;
                SpecificArtistController.super.service(req, resp);
            }
        };
        service(req, resp, superProxy); //(rq, rp) -> super.service(rq, rp));
    }

    @Override
    public void doPatch(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        Artist artistToChange;
        BufferedReader bufferedReader = req.getReader();
        Stream<String> lines = bufferedReader.lines();
        String jsonBody = lines.collect(Collectors.joining("\n"));
        try {
            UUID artistUuid = this.getUUIDFromURLPathInfo(req);
            ArtistService artistService = new ArtistService(new ArtistStorageMySQL());
            List<Artist> listContainingArtistToChange = artistService.getArtistList(artistUuid);
            if (listContainingArtistToChange.size() == 0) {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            else {
                  artistToChange = listContainingArtistToChange.get(0);
            }
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            ArtistDto partialArtist = objectMapper.readValue(jsonBody, ArtistDto.class);
            ArtistDtoMapper artistDtoMapper = Mappers.getMapper(ArtistDtoMapper.class);
            artistDtoMapper.updateArtistFromArtistDto(partialArtist, artistToChange);
            boolean artistUpdated = artistService.updateArtist(artistToChange);
            if (!artistUpdated) {
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
            else {
                resp.setStatus(HttpServletResponse.SC_OK);
            }
        }
        catch (IllegalArgumentException  | JsonProcessingException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
        catch (Exception e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            UUID artistUuid = this.getUUIDFromURLPathInfo(req);
            ArtistService artistService = new ArtistService(new ArtistStorageMySQL());
            boolean artistDeleted = artistService.deleteArtist(artistUuid);
            if (! artistDeleted) {
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
            else {
                resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
        catch (IllegalArgumentException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
        catch (Exception e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
