package infrastructure.jackson;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

public class EventJackson {
    private UUID id;
    private LocalDate startDate;
    private String description;
    @JsonCreator
    public EventJackson(@JsonProperty("id") UUID id, @JsonProperty("startDate") LocalDate startDate, @JsonProperty("description") String description) {
        this.id = id;
        this.startDate = startDate;
        this.description = description;
    }

    public UUID getId() {
        return id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = Objects.requireNonNull(description);
    }
}
