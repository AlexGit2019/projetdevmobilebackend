package infrastructure.jackson;

import domaine.Event;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper
public interface EventMapper {
    Event eventJacksonToEvent(EventJackson eventJackson);
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateEventFromEventJackson(EventJackson eventJackson, @MappingTarget Event event);

}
