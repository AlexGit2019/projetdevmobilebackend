package infrastructure.jackson;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import domaine.Artist;
import domaine.Event;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class ArtistJackson {
    private UUID id;
    private String name;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate careerStart;
    private List<Event> events;
    private String description;

    @JsonCreator
    public ArtistJackson(@JsonProperty("name") String name, @JsonProperty("careerStart") LocalDate careerStart, @JsonProperty("description") String description, @JsonProperty("events") List<Event> events) {
        this.name = Objects.requireNonNull(name);
        this.careerStart = Objects.requireNonNull(careerStart);
        this.events = Objects.requireNonNull(events);
        this.description = Objects.requireNonNull(description);
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDate getCareerStart() {
        return careerStart;
    }

    public List<Event> getEvents() {
        return events;
    }

    public String getDescription() {
        return description;
    }
}
