package infrastructure.jackson;

import domaine.Artist;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper
public interface ArtistJacksonMapper {
    Artist ArtistJacksonToArtist(ArtistJackson artistJackson);
}
