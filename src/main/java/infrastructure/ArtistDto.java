package infrastructure;

import com.fasterxml.jackson.annotation.JsonProperty;
import domaine.Event;

import java.time.LocalDate;
import java.util.List;

public class ArtistDto {
    private String name;
    private LocalDate careerStart;
    private List<Event> events;
    private String description;

    public ArtistDto(@JsonProperty("name") String name, @JsonProperty("careerStart") LocalDate careerStart, @JsonProperty("events") List<Event> events, @JsonProperty("description") String description) {
        this.name = name;
        this.careerStart = careerStart;
        this.events = events;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getCareerStart() {
        return careerStart;
    }

    public void setCareerStart(LocalDate careerStart) {
        this.careerStart = careerStart;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
