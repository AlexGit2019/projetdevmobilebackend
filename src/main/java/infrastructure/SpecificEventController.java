package infrastructure;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import domaine.Event;
import infrastructure.jackson.EventJackson;
import infrastructure.jackson.EventMapper;
import infrastructure.mysql.EventStorageMySQL;
import org.mapstruct.factory.Mappers;
import services.EventService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class SpecificEventController extends ItemRelatedHttpServlet implements PatchInterface {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletService servletServiceObject = new ServletService() {
            @Override
            public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                SpecificEventController.super.service(req, resp);
            }
        };
        service(req,resp,servletServiceObject);
    }

    @Override
    public void doPatch(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BufferedReader bufferedReader = req.getReader();
        try {
            UUID eventUuid = this.getUUIDFromURLPathInfo(req);
            EventService eventService = new EventService(new EventStorageMySQL());
            List<Event> listContainingEventToChange = eventService.getEvents(eventUuid);
            if (listContainingEventToChange.size() == 0) {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            }
            else {
                Event eventToChange = listContainingEventToChange.get(0);
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());
                EventJackson eventJackson = objectMapper.readValue(bufferedReader, EventJackson.class);
                EventMapper eventMapper = Mappers.getMapper(EventMapper.class);
                eventMapper.updateEventFromEventJackson(eventJackson, eventToChange) ;
                boolean eventUpdated = eventService.updateEvent(eventToChange);
                if (! eventUpdated) {
                    resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
                else {
                    resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
            }
        }
        catch (IllegalArgumentException | JsonProcessingException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            UUID eventUuid = this.getUUIDFromURLPathInfo(req);
            EventService eventService = new EventService(new EventStorageMySQL());
            boolean eventDeleted = eventService.deleteArtist(eventUuid);
            if (! eventDeleted) {
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
            else {
                resp.setStatus(HttpServletResponse.SC_OK);
            }
        }
        catch (IllegalArgumentException e) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        catch (Exception e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
