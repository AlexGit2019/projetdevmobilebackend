package infrastructure;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import domaine.Event;
import domaine.ServerException;
import infrastructure.jackson.EventJackson;
import infrastructure.jackson.EventMapper;
import infrastructure.mysql.EventStorageMySQL;
import org.mapstruct.factory.Mappers;
import services.EventService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class EventController extends ItemRelatedHttpServlet implements ServletErrorMessageInterface {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=UTF-8");
        UUID specifiedEventId;
        String idString = req.getParameter("id");
        if (idString == null || idString.trim().equals("")) {
            specifiedEventId = null;
        }
        else {
            try {
                specifiedEventId = UUID.fromString(idString);
            }
            catch (IllegalArgumentException e) {
                resp.sendError(400);
                return;
            }
        }
        try {
            EventService eventService = new EventService(new EventStorageMySQL());
            List<Event> eventList = eventService.getEvents(specifiedEventId);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            byte[] eventListBytes = objectMapper.writeValueAsBytes(eventList); //Sérialise la liste d'évènements sous la forme d'un tableau d'octets dont l'encodage est UTF-8
            resp.getOutputStream().write(eventListBytes);
        }
        catch (ServerException se) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BufferedReader bufferedReader = req.getReader();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        try {
            EventJackson eventJacksonToInsert = objectMapper.readValue(bufferedReader, EventJackson.class);
            eventJacksonToInsert.setId(UUID.randomUUID());
            EventMapper eventMapper = Mappers.getMapper(EventMapper.class);
            Event eventToInsert = eventMapper.eventJacksonToEvent(eventJacksonToInsert);
            EventService eventService = new EventService(new EventStorageMySQL());
            boolean eventInserted = eventService.addEvent(eventToInsert);
            if (!eventInserted) {
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ServletErrorMessageInterface.insertionFailedErrorMessage);
            }
            else {
                resp.setStatus(HttpServletResponse.SC_CREATED);
            }
        }
        catch (JsonProcessingException e) {
            resp.sendError(400,ServletErrorMessageInterface.incorrectJsonSentErrorMessage);
        }
        catch (Exception e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
