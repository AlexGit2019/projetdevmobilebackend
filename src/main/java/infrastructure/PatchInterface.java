package infrastructure;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface PatchInterface {
    default void service(HttpServletRequest req, HttpServletResponse resp, ServletService superObj) throws ServletException, IOException {
        if (req.getMethod().equals("PATCH")) {
            this.doPatch(req,resp);
        }
        else {
            superObj.service(req, resp);
        }
    }

    void doPatch(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;
}
