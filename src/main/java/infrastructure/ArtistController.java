package infrastructure;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import domaine.Artist;
import infrastructure.jackson.ArtistJackson;
import infrastructure.jackson.ArtistJacksonMapper;
import infrastructure.mysql.ArtistStorageMySQL;
import org.mapstruct.factory.Mappers;
import services.ArtistService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

public class ArtistController extends HttpServlet implements ServletErrorMessageInterface {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=UTF-8");
        String idString = req.getParameter("id");
        UUID specifiedArtistId;
        if (idString == null || idString.trim().equals("")) {
            specifiedArtistId = null;
        }
        else {
            try {
                specifiedArtistId = UUID.fromString(idString);
            }
            catch (IllegalArgumentException e) {
                resp.sendError(400);
                return;
            }
        }
        try {
            ArtistService artistService = new ArtistService(new ArtistStorageMySQL());
            List<Artist> artistList = artistService.getArtistList(specifiedArtistId);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            OutputStream writer = resp.getOutputStream();
            writer.write(objectMapper.writeValueAsBytes(artistList));
        }
        catch (Exception e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        BufferedReader bufferedReader = req.getReader();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        try {
            ArtistJackson artistJacksonToInsert = objectMapper.readValue(bufferedReader, ArtistJackson.class);
            artistJacksonToInsert.setId(UUID.randomUUID());
            ArtistJacksonMapper artistMapper = Mappers.getMapper(ArtistJacksonMapper.class);
            Artist artistToInsert = artistMapper.ArtistJacksonToArtist(artistJacksonToInsert);
            ArtistService artistService = new ArtistService(new ArtistStorageMySQL());
            boolean insertionSucceeded = artistService.insertArtist(artistToInsert);
            if (!insertionSucceeded) {
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ServletErrorMessageInterface.insertionFailedErrorMessage) ;
            }
            else {
                resp.setStatus(HttpServletResponse.SC_CREATED);
            }
        }
        catch (JsonProcessingException exception ) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST,ServletErrorMessageInterface.incorrectJsonSentErrorMessage);
            return;
        }
        catch (Exception e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }
    }

}
