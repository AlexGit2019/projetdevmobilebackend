package infrastructure;

import domaine.ClientException;
import domaine.ServerException;
import domaine.User;
import infrastructure.mysql.UserStorageMySQL;
import org.apache.commons.codec.digest.DigestUtils;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginController extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String login = req.getParameter("login");
        String password = req.getParameter("passwd");
        String passwdHashe = DigestUtils.sha256Hex(password);
        try {

            User user = new User(login,passwdHashe);
            UserService userService = new UserService(new UserStorageMySQL());
            Boolean authentificationSuccessful = userService.authenticateUser(user);

            if (authentificationSuccessful) {

                resp.setStatus(200);
                //resp.sendRedirect(req.getContextPath() + "/Acceuil.jsp");
                //resp.getWriter().write("Authentification Successful !");
                // TODO génération du token en cas de réussite
            }
            else {
                //TODO Gérer la redirection vers la page de connexion en cas de Fail
                resp.getWriter().write("Authentification failed !");
            }
        }
        catch (ServerException e) {
            resp.sendError(500,e.getMessage());
        }
        catch (ClientException e) {
            resp.sendError(400,e.getMessage());
        }
        catch (Exception e) {
            resp.sendError(500,e.getMessage());
        }
    }
}
