package infrastructure;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

public class ItemRelatedHttpServlet extends HttpServlet {
    protected UUID getUUIDFromURLPathInfo(HttpServletRequest req) throws IllegalArgumentException  {
        String pathInfo = req.getPathInfo();
        String[] arrayStringArtistUuid = pathInfo.split("/");
        String stringArtistUuid = arrayStringArtistUuid[1];
        return UUID.fromString(stringArtistUuid);
    }
}
