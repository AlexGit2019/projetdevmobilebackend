package infrastructure;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@FunctionalInterface
public interface ServletService {
    void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;
}
