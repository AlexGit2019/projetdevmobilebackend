package infrastructure;

import domaine.Artist;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ArtistDtoMapper {
    void updateArtistFromArtistDto(ArtistDto artistDto, @MappingTarget Artist artist);
}
