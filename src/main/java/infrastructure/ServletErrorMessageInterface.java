package infrastructure;

public interface ServletErrorMessageInterface {
    String incorrectJsonSentErrorMessage = "The sent JSON is incorrect";
    String insertionFailedErrorMessage = "The insertion failed";
}
