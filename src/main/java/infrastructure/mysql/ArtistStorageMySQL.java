package infrastructure.mysql;

import domaine.Artist;
import domaine.ClientException;
import domaine.Event;
import domaine.ServerException;
import services.ArtistStorageInterface;

import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ArtistStorageMySQL implements ArtistStorageInterface {
    private static String databaseAccessErrorMessage = "La base de données n'est pas accessible";
    private ConnexionMySQL connexionMySQL;
    private Connection connection;

    public ArtistStorageMySQL() {
        this.connexionMySQL = ConnexionMySQL.CONNECTION;
        this.connection = connexionMySQL.getConnection();
    }
    @Override
    public List<Artist> getArtistsList(UUID specifiedArtistId) {
        String query;
        ArrayList artists = new ArrayList<Artist>();
        if (specifiedArtistId == null) {
            query = "select * from artist;";
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                while (resultSet.next()) {
                    Artist artist = new Artist(UUID.fromString(resultSet.getString("id")), resultSet.getString("a_name"), resultSet.getObject("career_start", LocalDate.class), resultSet.getString("adescription"), new ArrayList<Event>());
                    artists.add(artist);
                }
            }
            catch (SQLException e) {
                throw new ServerException(ArtistStorageMySQL.databaseAccessErrorMessage);
            }
        }
        else {
            query = "select * from artist where id = ?;";
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, specifiedArtistId.toString());
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    Artist artist = new Artist(UUID.fromString(resultSet.getString("id")), resultSet.getString("a_name"), resultSet.getObject("career_start", LocalDate.class), resultSet.getString("adescription"), new ArrayList<Event>());
                    artists.add(artist);
                }
            }
            catch (SQLException e) {
                throw new ServerException(ArtistStorageMySQL.databaseAccessErrorMessage);
            }
        }
        return artists;
    }

    @Override
    public boolean insertArtist(Artist artistToInsert) {
        String query = "insert into artist(id, a_name, career_start, adescription) values (?,?, ?, ?);";
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement(query);
            preparedStatement.setString(1, artistToInsert.getId().toString());
            preparedStatement.setString(2, artistToInsert.getName());
            preparedStatement.setObject(3, artistToInsert.getCareerStart());
            preparedStatement.setString(4, artistToInsert.getDescription());
            int numberOfRowsAffected = preparedStatement.executeUpdate();
            return numberOfRowsAffected != 0;
        }
        catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean updateArtist(Artist artist) {
        String query = "update artist set a_name = ?, career_start = ?, adescription = ? where id = ?";
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement(query);
            preparedStatement.setString(1, artist.getName());
            preparedStatement.setObject(2, artist.getCareerStart());
            preparedStatement.setString(3, artist.getDescription());
            preparedStatement.setString(4, artist.getId().toString());
            preparedStatement.executeUpdate();
            return true;
        }
        catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean deleteArtist(UUID artistUuid) {
        String query = "delete from artist where id = ?;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, artistUuid.toString());
            int numberOfDeletedRows = preparedStatement.executeUpdate();
            return numberOfDeletedRows != 0;
        }
        catch (SQLException e) {
            return false;
        }
    }
}
