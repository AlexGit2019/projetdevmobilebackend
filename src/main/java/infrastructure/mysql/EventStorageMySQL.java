package infrastructure.mysql;

import domaine.Artist;
import domaine.ClientException;
import domaine.Event;
import domaine.ServerException;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class EventStorageMySQL implements services.EventStorageInterface {
    public ConnexionMySQL CONNECTION;
    public Connection connection;

    public EventStorageMySQL() {
        try {
            this.CONNECTION = ConnexionMySQL.CONNECTION;
            this.connection = CONNECTION.getConnection();
        }
        catch (IllegalStateException e) {
            throw new ServerException("Erreur de connexion à la BDD");
        }
    }

    public List<Event> getEvents(UUID specifiedEventId) {
        String queryBase = "select * from festival_event";
        List<Event> events = new ArrayList<>();
        if (specifiedEventId == null ) {
            String query = queryBase + ";";
            try {
                Statement statement = this.connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                while (resultSet.next()) {
                    Event event = new Event(UUID.fromString(resultSet.getString("id")), resultSet.getObject("start_date", LocalDate.class) ,resultSet.getString("edescription"));
                    events.add(event);
                }
                return events;
            }
            catch (SQLException e) {
                throw new ServerException("Erreur de connexion à la BDD", e);
            }
        }
        else {
            String query = queryBase + " where id = ? ;";
            try {
                PreparedStatement preparedStatement = this.connection.prepareStatement(query);
                preparedStatement.setString(1, specifiedEventId.toString());
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    Event event = new Event(UUID.fromString(resultSet.getString("id")), resultSet.getObject("start_date", LocalDate.class) ,resultSet.getString("edescription"));
                    events.add(event);
                }
                return events;
            }
            catch (SQLException e) {
                throw new ServerException("Erreur de connexion à la BDD", e);
            }
        }
    }

    @Override
    public boolean addEvent(Event eventToInsert) {
        String query = "insert into festival_event(id, edescription,start_date) values (?,?, ?);";
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement(query);
            preparedStatement.setString(1, eventToInsert.getId().toString());
            preparedStatement.setString(2, eventToInsert.getDescription());
            preparedStatement.setObject(3, eventToInsert.getStartDate());
            int numberOfRowsAffected = preparedStatement.executeUpdate();
            return numberOfRowsAffected != 0;
        }
        catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean updateEvent(Event event) {
        try {
            String query = "update festival_event set edescription = ?, start_date = ? where id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, event.getDescription());
            preparedStatement.setObject(2,event.getStartDate());
            preparedStatement.setString(3, event.getId().toString());
            int numberOfRowsAffected = preparedStatement.executeUpdate();
            return numberOfRowsAffected != 0;
        }
        catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean deleteArtist(UUID eventUuid) {
        try {
            String query = "delete from festival_event where id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, eventUuid.toString());
            int numberOfRowsAffected = preparedStatement.executeUpdate();
            return numberOfRowsAffected != 0;
        }
        catch (SQLException e) {
            return false;
        }
    }
}
