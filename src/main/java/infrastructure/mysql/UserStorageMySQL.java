package infrastructure.mysql;

import domaine.ServerException;
import domaine.User;
import services.UserStorageInterface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserStorageMySQL implements UserStorageInterface {
    private ConnexionMySQL CONNECTION;
    private Connection connection;

    public UserStorageMySQL() {
        this.CONNECTION = ConnexionMySQL.CONNECTION;
        connection = this.CONNECTION.getConnection();
    }

    @Override
    public Boolean authenticateUser(User user) {
        String query = "SELECT * FROM users where login = ? and hashed_passwd = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1,user.getLogin());
            statement.setString(2,user.getHashedPassword());
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String login = resultSet.getString("login");
                String hashed_password = resultSet.getString("hashed_passwd");
                return true;
            }
            else {
                return false;
            }
        } catch (SQLException e) {
            throw new ServerException("Erreur de connexion à la BDD");
        }
    }
}
